from setuptools import setup, find_packages

setup(
    name='rabbitmqX',  # Required
    version='1.10.17',  # Required
    author="Paulo Sergio dos Santo Junior",
    author_email="paulossjuniort@gmail.com",
    description="Implementation RabbitMQ's Design Patterns ",
 
    packages=find_packages(),
    
    install_requires=[
        'pika'
    ],

    classifiers=[
         "Programming Language :: Python :: 3",
         "License :: OSI Approved :: MIT License",
         "Operating System :: OS Independent",
     ],
    setup_requires=['wheel'],
    
)