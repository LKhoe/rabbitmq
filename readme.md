# Objetivo

Responsável por realizar a comunicação com o [RabbitMQ](https://www.rabbitmq.com/) por meio dos padrões (e.g., RPC e Work Task) disponibilizados na [documentação](https://www.rabbitmq.com/getstarted.html). 

## Padrão cliente e Servidor
Para cada padrão foram desenvolvidos duas classes: cliente e servidor. Além disso, as implementações tratam cada requisição em thread distintas. Dessa forma, não bloqueando a aplicação principal, caso um job "trave"

Exemplo de instanciação da classe servidora do padrão RPC.

```python
from rabbitmqX.patterns.server.rpc_server import RPC_Server
x = RPC_Server("X", None)
x.start()
```

## Instalação
pip install rabbimqX